<?php
abstract class AdminController{
    protected $user;

    public function __construct()
    {

        if(array_key_exists("user", $_SESSION)){
            $this->user = unserialize($_SESSION["user"]);
        } else {
            header('Location: index.php?controller=security&action=login');
        }
    }
}