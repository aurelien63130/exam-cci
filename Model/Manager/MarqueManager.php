<?php
    class MarqueManager extends DbManager {
        public function findAll(){
            $query = $this->bdd->prepare("SELECT * FROM marque");
            $query->execute();

            $resultats = $query->fetchAll();
            $arrayObject = [];

            foreach ($resultats as $resultat){
                $arrayObject[] = new Marque($resultat["id"], $resultat["nom"]);
            }

            return $arrayObject;
        }
    }
?>