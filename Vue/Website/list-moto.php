<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'Vue/Parts/global-stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
        include 'Vue/Website/menu-website.php';
    ?>
    <h1>Les motos de l'appli !</h1>

    <div class="row">
    <?php
     foreach ($motos as $moto){
         $img = '';
         if(!empty($moto->getImage()))
         {
             $img = 'Public/uploads/'.$moto->getImage();
         }        else {
             $img = 'Public/imgs/no-picture.png';
         }

         echo('<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="'.$img.'" >
  <div class="card-body">
    <h5 class="card-title">'.$moto->getModel().'</h5>
    <a href="index.php?controller=website&action=detail&id='.$moto->getId().'" class="btn btn-primary">Voir plus !</a>
  </div>
</div>');
     }
    ?>
    </div>
</div>


<?php
require 'Vue/Parts/global-scripts.php';
?>

</body>
</html>