<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'Vue/Parts/global-stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
    include 'Vue/Website/menu-website.php';
    ?>

    <h1>La moto <?php echo($moto->getModel());?> !</h1>
    <?php
    $img = '';
    if(!empty($moto->getImage()))
    {
        $img = 'Public/uploads/'.$moto->getImage();
    }        else {
        $img = 'Public/imgs/no-picture.png';
    }
    echo('<img src="'.$img.'"><br><h2>'.$moto->getType().' De marque '.$moto->getMarque()->getNom().'</h2>');
    ?>

</div>


<?php
require 'Vue/Parts/global-scripts.php';
?>

</body>
</html>