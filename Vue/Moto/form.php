<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'Vue/Parts/global-stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
        if(isset($editMoto)){
            echo('<h1>Edition de la moto '.$editMoto->getModel().' !</h1>');
        } else {
            echo('<h1>Ajouter une moto !</h1>');
        }
    ?>

    <a href="index.php?controller=moto&action=list">Revenir au listing !</a>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="form-label" for="marque">Marque</label>
            <select class="form-control" name="marque" id="marque">
                <option></option>
                <?php
                    foreach ($marques as $marque){
                        $strSelected = '';
                        if(isset($editMoto) &&  $marque->getId() == $editMoto->getMarque()->getId()){
                            $strSelected = 'selected';
                        }
                        echo('<option '.$strSelected.' value="'.$marque->getId().'">'.$marque->getNom().'</option>');
                    }
                ?>
            </select>
        </div>

            <div class="form-group">
                <label class="form-label" for="modele">Modele</label>
                <input <?php if(isset($editMoto)){echo('value="'.$editMoto->getModel().'"');}?> type="text" name="modele" class="form-control" id="modele" placeholder="CBR">
            </div>

        <div class="form-group">
            <label class="form-label" for="type">Type</label>
            <select class="form-control" name="type" id="marque">
                <option></option>
                <option <?php if(!empty($editMoto) && $editMoto->getType() == 'Enduro'){echo('selected');}?>>Enduro</option>
                <option <?php if(!empty($editMoto) && $editMoto->getType() == 'Custom'){echo('selected');}?> value="Custom">Custom</option>
                <option <?php if(!empty($editMoto) && $editMoto->getType() == 'Sportive'){echo('selected');}?> value="Sportive">Sportive</option>
                <option <?php if(!empty($editMoto) && $editMoto->getType() == 'Roadster'){echo('selected');}?> value="Roadster">Roadster</option>
            </select>
        </div>

        <div class="form-group">
            <label for="formFile" class="form-label">Image de la moto</label>
            <input class="form-control" name="picture" type="file" id="formFile">

            <?php
                if(!empty($editMoto) && !empty($editMoto->getImage())){
                    echo('Image actuelle :<br> <img src="Public/uploads/'.$editMoto->getImage().'">');
                }
            ?>
        </div>

        <?php
            require 'Vue/Parts/form-error.php'
        ?>

        <input type="submit" class="mt-2 btn btn-success">

        </div>


    </form>
</div>


<?php
require 'Vue/Parts/global-scripts.php';
?>

</body>
</html>