<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'Vue/Parts/global-stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
        include 'Vue/Parts/menu.php';
    ?>
    <h1>Le listing des motos ! </h1>

    <a href="index.php?controller=moto&action=add">Ajouter une moto !</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Marque</th>
            <th scope="col">Modele</th>
            <th scope="col">Type</th>
            <th scope="col">Image</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($motos as $resultat) {
            $img = '';
            if(!empty($resultat->getImage()))
            {
                $img = 'Public/uploads/'.$resultat->getImage();
            }        else {
                $img = 'Public/imgs/no-picture.png';
            }

            echo(' <tr>
            <th scope="row">'.$resultat->getId().'</th>
            <td>'.$resultat->getMarque()->getNom().'</td>
            <td>'.$resultat->getModel().'</td>
            <td>'.$resultat->getType().'</td>
             <td>
                
                <img class="img-thumbnail img_preview" src="'.$img.'">
             </td>
             
              <td>
                <a href="index.php?controller=moto&action=edit&id='.$resultat->getId().'">Editer</a> | 
                <a href="index.php?controller=moto&action=remove&id='.$resultat->getId().'">Supprimer</a>
             </td>
             
        </tr>');
        }
        ?>

        </tbody>
    </table>

</div>


<?php
require 'Vue/Parts/global-scripts.php';
?>

</body>
</html>