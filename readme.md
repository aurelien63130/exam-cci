###### ****Examen (3 h 30):****

**1) Partie théorique :** 

- Expliquer ce qu'est un constructeur (comment l'écrire, quand est il appelé) (**2 points**)

- Expliquer ce qu'est un destructeur (comment l'écrire, quand est il appelé) (**2 points**) 

- Expliquer les 3 types de visibilités étudiés en cours (Expliquez en détail leur fonctionnement) (**2 points**)

**2) Partie pratique :** 

1) Créer une nouvelle BDD à partir du script SQL ci-dessous : 

````mysql
CREATE TABLE `marque` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `marque` (`id`, `nom`) VALUES (1,'Kawasaki');
INSERT INTO `marque` (`id`, `nom`) VALUES (2,'Suzuki');
INSERT INTO `marque` (`id`, `nom`) VALUES (3,'KTM');
INSERT INTO `marque` (`id`, `nom`) VALUES (4,'Sherco');
INSERT INTO `marque` (`id`, `nom`) VALUES (5,'Yamaha');
INSERT INTO `marque` (`id`, `nom`) VALUES (6,'Harley Davidson');

CREATE TABLE `moto` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `id_marque` int NOT NULL,
                        `modele` varchar(250) NOT NULL,
                        `type` varchar(250) NOT NULL,
                        `lien_image` varchar(250) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `moto_marque_id_fk` (`id_marque`),
                        CONSTRAINT `moto_marque_id_fk` FOREIGN KEY (`id_marque`) REFERENCES `marque` (`id`)
);

INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (4,2,'GSXR 1000 R','Sportive','6231bd4424938.jpeg');
INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (5,3,'250 EXC','Enduro','6231bd9d99a3f.jpeg');
INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (6,4,'300 SE Factory','Enduro','6231be7134f00.jpeg');
INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (7,1,'Z900','Roadster','6231beb740801.jpeg');
INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (8,5,'MT09','Roadster','6231bf35e8b48.jpeg');
INSERT INTO `moto` (`id`, `id_marque`, `modele`, `type`, `lien_image`) VALUES (9,6,'Sportster S 2022','Custom','6231c01ddb3a7.jpeg');


CREATE TABLE `utilisateur` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `username` varchar(250) DEFAULT NULL,
                               `password` varchar(250) DEFAULT NULL,
                               PRIMARY KEY (`id`)
);

INSERT INTO `utilisateur` (`id`, `username`, `password`) VALUES (1,'cci','$2y$10$iMnAfCdd9X1qoisriQkqGeTOjso8lrP8rx0AEc3X9gDTi5mKD6I8S');


````
Le script contient des données relative au fonctionnement du site et un utilisateur : <br>
**identifiant** : cci -> **mot de passe** : cci43

2) Renseigner les identifiants de votre BDD dans le fichier adéquat (**2 points**)

**TODO LIST (Remplacer les extraits de code manquants) :** 

_ROUTEUR :_ 

throw new Exception("TODO ! ROUTEUR DU LOGIN"); --> index.php (**2 points**)

_VUE :_ 

<!-- TODO : Faire le formulaire de LOGIN --> Vue/Security/login.php ( 2 points )
<br>
<!-- TODO : Affichage d'un lien pour voir toutes les motos --> Vue/Parts/menu.php (2 point)

_Model :_ 

throw new Exception("TODO : Développer une méthode qui retourne toutes les motos de notre BDD"); --> Model/Manager/MotoManager.php (**1 point**)<br>
throw new Exception("TODO : Retourner un utilisateur en fonction de son username"); --> Model/Manager/UtilisateurManager.php (**1 point**)<br>
<br><br>
_Controller :_ 

throw new Exception("TODO : Suppression de la moto en fonction de son id"); --> Controller/MotoController.php (**2 points**)<br>
<br>
Rendu Sur un repo privé GitHub (M'ajouter en tant que collaborateur identifiant : Aurelien16974) (**2 points**) 

